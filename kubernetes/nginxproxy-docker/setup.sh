#!/bin/bash

#creer  user
kubectl apply -f nginxuser.yml

#/!\ Setup des clef et cert ssl /!\ doit être modifié avec les bonnes clef et bon  nom de projet!
kubectl apply -f nginx-certificate-configmap.yml

#creation du pod
kubectl apply -f nginx-proxy-pod.yml

#Rediriger le DNS sur ce service
kubectl apply -f nginx-proxy-service.yml



Printf "\n------------\n"
Printf "Liste des annotations a ajouter dans les .yml des projets\n"
Printf "Cf : https://hub.docker.com/r/robertdolca/kubernetes-nginx-proxy\n"

# Domain name used for the virtual host
Printf "Nom de domaine attendu: proxy_host: example.com\n"

# Path to proxy
Printf "Chemin vers le proxy (laisser sur /) proxy_path: /  \n"

# This flag enables web sockets
Printf "Activer/Desactiver les websockets proxy_web_socket: 'false' \n"

# Enable http proxy
Printf "Activer le proxy en http: proxy_http: 'true'\n"

# Enable https proxy and certificate configuration
Printf "Activer le proxy en https proxy_https: 'true'\n"
Printf "Ajouter le certificat https  (Dans nginx-certificate-configmap.yml): proxy_ssl_cert: example.com/fullchain.pem\n"
Printf "Ajouter la clef https  (Dans nginx-certificate-configmap.yml): proxy_ssl_key: example.com/privkey.pem\n"

# Redirect http to https
Printf "Activer la redirection http vers https : proxy_https_redirect: 'true'\n"
