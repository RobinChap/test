#!/bin/bash

export KUBECONFIG=/Users/admin/.kube/kube-config

kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/master/aio/deploy/recommended/kubernetes-dashboard.yaml

kubectl apply -f dashboard-service-account.yml

kubectl apply -f dashboard-cluster-role-binding.yml

printf "\n-------TOKEN-----------\n" 

kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep admin-user-token | awk '{print $1}')

printf "\n-------TOKEN-----------\n"

printf "\n-------URL-----------\n"
printf "http://localhost:8001/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:/proxy"
printf "\n-------URL-----------\n"
kubectl proxy
